import {AppState} from "./index";
import {ActionReducer} from "@ngrx/store";

export const loggerMetaReducer = (reducer: ActionReducer<AppState>) => {
  return (state, action) => {
    const newState = reducer(state, action);
    console.group(action.type);
    console.log(`%c prev state`, `color: #9E9E9E; font-weight: bold`, state);
    console.log(`%c action`, `color: #03A9F4; font-weight: bold`, action);
    if (newState !== state) {
      console.log(`%c next state`, `color: #4CAF50; font-weight: bold`, newState);
    } else {
      console.log(`%c state is not changed`, `color: #4CAF50; font-weight: bold`);
    }
    console.groupEnd();
    return newState;
  };
};

import { TodoItemState }  from './todo.state'
import TodoItem from '../../model/todo.model'

import {Action} from '@ngrx/store'

export const CREATE_TODO_ITEM = '[Todo] CREATE_TODO_ITEM'
export const CREATE_TODO_ITEM_SUCCESS = '[Todo] CREATE_TODO_ITEM_SUCCESS'
export const CREATE_TODO_ERROR = '[Todo] CREATE_TODO_ERROR'

export const GET_TODO_ITEM = '[Todo] GET_TODO_ITEM'
export const GET_TODO_ITEM_SUCCESS = "[Todo] GET_TODO_ITEM_SUCCESS"
export const GET_TODO_ITEM_ERROR = "[Todo] GET_TODO_ITEM_ERROR"

export const EDIT_TODO_ITEM = '[Todo] EDIT_TODO_ITEM';

export const UPDATE_TODO_ITEM = '[Todo] UPDATE_TODO_ITEM'
export const UPDATE_TODO_ITEM_SUCCESS = '[Todo] UPDATE_TODO_ITEM_SUCCESS'
export const UPDATE_TODO_ITEM_ERROR = '[Todo] UPDATE_TODO_ITEM_ERROR'

export const GET_TODO_ITEM_LIST = '[Todo] GET_TODO_ITEM_LIST'
export const GET_TODO_ITEM_LIST_SUCCESS = '[Todo] GET_TODO_ITEM_LIST_SUCCESS'
export const GET_TODO_ITEM_LIST_ERROR = '[Todo] GET_TODO_ITEM_LIST_ERROR'

export const DELETE_TODO_ITEM = '[Todo] DELETE_TODO_ITEM'
export const DELETE_TODO_ITEM_SUCCESS = '[Todo] DELETE_TODO_ITEM_SUCCESS'
export const DELETE_TODO_ITEM_ERROR = '[Todo] DELETE_TODO_ITEM_ERROR'

export const COMPLETE_TODO_ITEM = 'COMPLETE_TODO_ITEM'

export class GetTodoItemList implements Action {
    readonly type = GET_TODO_ITEM_LIST
}

export class GetTodoItemListSuccess implements Action {
    readonly type = GET_TODO_ITEM_LIST_SUCCESS

    constructor(public todoItemList: TodoItem[]){}

}
export class GetTodoItemListError implements Action {
    readonly type = GET_TODO_ITEM_LIST_ERROR
}
// Action for Creating TOdos
export class CreateTodoItem implements Action {
    readonly type = CREATE_TODO_ITEM

    constructor(public itemToCreate: TodoItem){}
}
export class CreateTodoItemSuccess implements Action {
    readonly type = CREATE_TODO_ITEM_SUCCESS

    constructor(public createdItem: TodoItem){}
}
export class CreateTodoError implements Action {
    readonly type = CREATE_TODO_ERROR
}

export class GetTodoItem implements Action {
    readonly type = GET_TODO_ITEM

    constructor(public id: string){}
}

export class GetTodoItemSuccess implements Action {
    readonly type = GET_TODO_ITEM_SUCCESS

    constructor(public payload: TodoItem){}

}

export class GetTodoItemError implements Action {
    readonly type = GET_TODO_ITEM_ERROR
}
export class CompleteTodoItem implements Action {
    readonly type = COMPLETE_TODO_ITEM

    constructor(public id: string){}

}

export class EditTodoItem implements Action {
  readonly type = EDIT_TODO_ITEM;
  constructor(public id: string){}
}


export class UpdateTodoItem implements Action {
    readonly type = UPDATE_TODO_ITEM

    constructor(public todoItem: TodoItem){}

}

export class UpdateTodoItemSuccess implements Action {
    readonly type = UPDATE_TODO_ITEM_SUCCESS

    constructor(public todoItem: TodoItem){
        console.log(this.todoItem)
    }

}

export class UpdateTodoItemError implements Action {
    readonly type = UPDATE_TODO_ITEM_ERROR
    constructor(public id: string, public details: string){}
}

export class DeleteTodoItem implements Action {
  readonly type = DELETE_TODO_ITEM

  constructor(public id: string){}
}

export class DeleteTodoItemSuccess implements Action {
    readonly type = DELETE_TODO_ITEM_SUCCESS

    constructor(public id: string){}
}
export class DeleteTodoItemError implements Action {
    readonly type = DELETE_TODO_ITEM_ERROR

    constructor(public id: string){}
}

export type All = GetTodoItem |EditTodoItem | GetTodoItemSuccess | GetTodoItemError |
UpdateTodoItem | UpdateTodoItemSuccess | UpdateTodoItemError |
GetTodoItemList | GetTodoItemListSuccess | GetTodoItemListError |
CreateTodoItem | CreateTodoItemSuccess | CreateTodoError |
DeleteTodoItem | DeleteTodoItemSuccess | DeleteTodoItemError |
CompleteTodoItem

import * as TodoState from './todo.state';
import * as TodoActions from './todo.action';
import {INITIAL_NEW_ITEM_STATE} from "./todo.state";

export function TodoReducer(state = TodoState.initialState, action: TodoActions.All) {

  let {loading, pending, itemIds, itemById, itemStateById, newItemState} = state;

  switch (action.type) {

    case TodoActions.CREATE_TODO_ITEM: {
      newItemState = {
        ...newItemState,
        saving: true
      };
      break;
    }

    case TodoActions.CREATE_TODO_ITEM_SUCCESS: {
      const createdItem = action.createdItem;
      const id = createdItem._id;
      const createdItemState = TodoState.LOADED_ITEM_STATE;
      itemById = {
        ...itemById,
        [id]: createdItem
      };
      itemStateById = {
        ...itemStateById,
        [id]: createdItemState
      };
      itemIds = [...itemIds, id];
      newItemState = INITIAL_NEW_ITEM_STATE;
      break;
    }

    case TodoActions.GET_TODO_ITEM_LIST: {
      loading = true;
      break;
    }


    case TodoActions.GET_TODO_ITEM_LIST_SUCCESS: {
      itemIds = [];
      itemById = {};
      itemStateById = {};
      loading = false;
      action.todoItemList.forEach(todoItem => {
        const id = todoItem._id;
        itemIds.push(id);
        itemById[id] = todoItem;
        itemStateById[id] = TodoState.LOADED_ITEM_STATE;
      });
      break;
    }

    case TodoActions.EDIT_TODO_ITEM: {
      const id: string = action.id;
      const itemState = itemStateById[id];

      const newItemState: TodoState.TodoItemState = {...itemState, editing: true};

      itemStateById = {...itemStateById, [id]: newItemState};
      break;
    }

    case TodoActions.DELETE_TODO_ITEM: {
      const id = action.id;

      itemIds = itemIds.filter(itemId => itemId !== id);
      itemById = {...itemById};
      itemStateById = {...itemStateById};
      delete itemById[id];
      delete itemStateById[id];

      break;
    }

    case TodoActions.UPDATE_TODO_ITEM: {
      const itemToUpdate = action.todoItem;
      const id = itemToUpdate._id;
      const newItemState = {...itemStateById[id], saving: true};
      itemById = {...itemById, [id]: itemToUpdate};
      itemStateById = {...itemStateById, [id]: newItemState}
      break;
    }

    case TodoActions.UPDATE_TODO_ITEM_SUCCESS: {
      const updatedItem = action.todoItem;
      const id = updatedItem._id;
      const newItemState = {...itemStateById[id], editing: false, saving: false};

      itemById = {...itemById, [id]: updatedItem};
      itemStateById = {...itemStateById, [id]: newItemState}
      break;
    }




    // case TodoActions.UPDATE_TODO_ITEM: {
    //   const itemToUpdate = action.todoItem;
    //   const id = itemToUpdate._id;
    // }


    // case TodoActions.UPDATE_TODO_ITEM_SUCCESS: {
    //
    //   return modifyTodoState(state, action.todoItem, {})
    // }


    // case TodoActions.UPDATE_TODO_ITEM_ERROR: {
    //
    //
    // }


    case TodoActions.COMPLETE_TODO_ITEM: {

      const newItem = {...itemById[action.id]};
      newItem.status = "done";
      itemById = {...itemById, [action.id]: newItem}
    }

    default: {
      return state;
    }
  }
  return {loading, pending, itemIds, itemById, itemStateById, newItemState};
}

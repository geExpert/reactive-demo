import {environment} from '../../../environments/environment';
import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {Actions, Effect} from '@ngrx/effects';

import * as TodoActions from './todo.action';

import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs/internal/Observable";
import {catchError, map, mergeMap} from "rxjs/operators";
import {of} from "rxjs/internal/observable/of";
import {default as TodoItem, TodoItemResponse, TodoItemsResponse} from "../../model/todo.model";
import {CreateTodoItem, DeleteTodoItem, UpdateTodoItem} from "./todo.action";

@Injectable()
export class TodoEffects {

  constructor(
    private http: HttpClient,
    private actions$: Actions
  ) {
  }


  @Effect()
  GetTodos$: Observable<Action> = this.actions$
    .ofType<TodoActions.GetTodoItemList>(TodoActions.GET_TODO_ITEM_LIST)
    .pipe(
      mergeMap(action =>
        this.http.get<TodoItemsResponse>(environment.client.base_url + '/api/todos')
          .pipe(
            map(response => response.data.docs),
            map(itemDocList => itemDocList.map(itemDoc => new TodoItem(itemDoc))),
            map((itemList) => {
              console.log(itemList);
              return new TodoActions.GetTodoItemListSuccess(itemList);
            }),
            catchError(() => of(new TodoActions.GetTodoItemError()))
          )
      )
    )
  ;

  @Effect()
  createTodo$: Observable<Action> = this.actions$.ofType<TodoActions.CreateTodoItem>(TodoActions.CREATE_TODO_ITEM)
    .pipe(mergeMap((action:CreateTodoItem) =>
      this.http.post<TodoItemResponse>(environment.client.base_url + '/api/todos', action.itemToCreate)
        .pipe(
          map((response: TodoItemResponse) => response.data),
          map(itemDoc => new TodoItem(itemDoc)),
          map((createdItem: TodoItem) => {
            return new TodoActions.CreateTodoItemSuccess(createdItem);
          }),
          catchError((err) => of(new TodoActions.CreateTodoError()))
        )
    ));

  @Effect()
  deleteTodo$: Observable<Action> = this.actions$.ofType<TodoActions.DeleteTodoItem>(TodoActions.DELETE_TODO_ITEM)
    .pipe(mergeMap((action: DeleteTodoItem) =>
      this.http.delete<TodoItemResponse>(environment.client.base_url + '/api/todos/' + action.id)
        .pipe(
          map((response) => {

            return new TodoActions.DeleteTodoItemSuccess(action.id);
          }),
          catchError(() => of(new TodoActions.DeleteTodoItemError(action.id)))
        )
    ))
  ;

  @Effect()
  updateTodo$: Observable<Action> = this.actions$.ofType<TodoActions.UpdateTodoItem>(TodoActions.UPDATE_TODO_ITEM)
    .pipe(
      mergeMap((action: UpdateTodoItem) =>
        this.http.put<TodoItemResponse>(environment.client.base_url + '/api/todos/', action.todoItem)
          .pipe(
            map((response: TodoItemResponse) => response.data),
            map(todoItemDoc => new TodoItem(todoItemDoc)),
            map(todoItem => {
              return new TodoActions.UpdateTodoItemSuccess(todoItem);
            })
            , catchError((err) => of(new TodoActions.UpdateTodoItemError(action.todoItem._id, err.toString())))
          )
      ))
  ;

}

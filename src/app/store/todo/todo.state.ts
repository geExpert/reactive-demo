import TodoItem, {EMPTY_TODO_ITEM} from '../../model/todo.model';
import {createFeatureSelector, createSelector, Store} from "@ngrx/store";
import {combineLatest} from "rxjs/internal/observable/combineLatest";
import {map} from "rxjs/operators";
import {AppState} from "../index";

export interface TodoItemState {
  loading: boolean;
  editing: boolean;
  saving: boolean;
}

export const NEW_ITEM_STATE: TodoItemState = {
  loading: false,
  editing: true,
  saving: false
};

export const LOADED_ITEM_STATE: TodoItemState = {
  loading: false,
  editing: false,
  saving: false
};

export interface TodoItemAndState {
  item: TodoItem;
  state: TodoItemState;
}

export interface NewItemState {
  item: TodoItem;
  saving: boolean;
}

export interface TodoListState {
  itemIds: string[];
  itemStateById: MapOf<TodoItemState>;
  itemById: MapOf<TodoItem>;
  newItemState: NewItemState;
  loading: boolean;
  pending: number;
}

export const INITIAL_NEW_ITEM_STATE: NewItemState = {
  item: EMPTY_TODO_ITEM,
  saving: false
};

export const initialState: TodoListState = {
  itemIds: [],
  itemById: {},
  itemStateById: {},
  newItemState : INITIAL_NEW_ITEM_STATE,
  loading: false,
  pending: 0
};

export interface MapOf<T> {
  [id: string]: T;
}

export const selectTodoFeature = createFeatureSelector<TodoListState>('todo');
export const selectItemIds = createSelector(selectTodoFeature, todo => todo.itemIds);
export const selectItemStateById = createSelector(selectTodoFeature, todo => todo.itemStateById);
export const selectItemById = createSelector(selectTodoFeature, todo => todo.itemById);
export const selectNumberOfItems = createSelector(selectItemIds, itemIds => itemIds.length);
export const selectLoading = createSelector(selectTodoFeature, todo => todo.loading);
export const selectNewItemState = createSelector(selectTodoFeature, todo => todo.newItemState);

export function selectItemAndStateList(state$: Store<AppState>) {

  return combineLatest(
    state$.select(selectItemIds),
    state$.select(selectItemById),
    state$.select(selectItemStateById))
    .pipe(
      map(([itemIds, itemById, stateById]) =>
        itemIds.map(
          id => ({
            item: itemById[id],
            state: stateById[id]
          })
        )
      )
    );

}

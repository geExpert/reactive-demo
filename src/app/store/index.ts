import {TodoListState} from "./todo/todo.state";

export interface AppState {
  todo: TodoListState;
}

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {TodoService} from "./service/todo.service";
import {HttpClientModule} from "@angular/common/http";
import {TodoListComponent} from "./components/todo/todo-list/todo-list.component";
import {TodoListItemComponent} from "./components/todo/todo-list-item/todo-list-item.component";

import * as TodoReducer from './store/todo/todo.reducer'
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {TodoEffects} from "./store/todo/todo.effects";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {environment} from "../environments/environment";
import {loggerMetaReducer} from "./store/meta-reducers";

const reducerMap = {todo: TodoReducer.TodoReducer};
const metaReducers = environment.production ? [] : [loggerMetaReducer];

@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    TodoListItemComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    StoreModule.forRoot(reducerMap, {metaReducers: metaReducers}),
    EffectsModule.forRoot([TodoEffects])
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})
export class AppModule {
}

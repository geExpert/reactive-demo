import {
  TodoListState,
  selectItemAndStateList,
  TodoItemAndState,
  selectLoading, selectNumberOfItems, NewItemState, selectNewItemState
} from '../../../store/todo/todo.state';

import {Store} from '@ngrx/store';


import {Component, OnInit} from '@angular/core';


import * as TodoAction from '../../../store/todo/todo.action';
import {Observable} from "rxjs/internal/Observable";
import {AppState} from "../../../store";
import {TodoItemId} from "../../../model/todo.model";


@Component({

  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  constructor(private store: Store<AppState>) {
  }

  itemAndStateList$: Observable<TodoItemAndState[]>;
  loading$: Observable<boolean>;
  numberOfItems$: Observable<number>;
  newItemState$: Observable<NewItemState>;

  ngOnInit() {
    this.itemAndStateList$ = selectItemAndStateList(this.store);
    this.loading$ = this.store.select(selectLoading);
    this.numberOfItems$ = this.store.select(selectNumberOfItems);
    this.newItemState$ = this.store.select(selectNewItemState);
    this.store.dispatch(new TodoAction.GetTodoItemList());
  }

  onCreate(todo) {
    this.store.dispatch(new TodoAction.CreateTodoItem(todo));
  }


  onDelete(id: TodoItemId) {
    this.store.dispatch(new TodoAction.DeleteTodoItem(id));
  }

  onEdit(id: TodoItemId) {
    this.store.dispatch(new TodoAction.EditTodoItem(id));
  }

  onSave(todoItem) {
    this.store.dispatch(new TodoAction.UpdateTodoItem(todoItem));
  }

  onCompleteTodo(id: TodoItemId) {
    this.store.dispatch(new TodoAction.CompleteTodoItem(id));
  }
}

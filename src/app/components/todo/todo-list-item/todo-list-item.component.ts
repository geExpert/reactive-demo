import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import TodoItem, {TodoItemId} from "../../../model/todo.model";
import {TodoItemState} from "../../../store/todo/todo.state";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-todo-list-item',
  templateUrl: './todo-list-item.component.html',
  styleUrls: ['./todo-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoListItemComponent implements OnInit {

  formGroup: FormGroup;
  id: TodoItemId;
  originalItem: TodoItem;


  @Input() state: TodoItemState;
  @Input() newItem: boolean;

  @Input() set todoItem(item: TodoItem) {
    this.id = item._id;
    this.originalItem = item;
    const formPart = {"title": item.title, "description": item.description};
    if (this.formGroup) {
      this.formGroup.setValue(formPart);
    } else {
      this.formGroup = this.formBuilder.group(formPart);
    }
  }

  @Input() set saving(saving: boolean) {
    if (this.state) {
      this.state.saving = saving;
    } else {
      this.state = {
        saving, loading: false, editing: true
      };
    }
  }

  @Output() create = new EventEmitter<TodoItem>();
  @Output() delete = new EventEmitter<TodoItemId>();
  @Output() save = new EventEmitter<TodoItem>();
  @Output() complete = new EventEmitter<TodoItemId>();
  @Output() edit = new EventEmitter<TodoItemId>();

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    if (this.newItem) {
      this.state = {
        editing: true,
        loading: false,
        saving: false
      };
    }
  }

  private getItem() {
    return {...this.formGroup.getRawValue(), _id: this.id, __v: this.originalItem.__v} as TodoItem;
  }

  editItem() {
    this.edit.emit(this.id);
  }

  completeItem() {
    this.complete.emit(this.id);
  }

  createNewItem() {
    this.create.emit(this.getItem());
  }

  saveItem() {
    this.save.emit(this.getItem());
  }

  deleteItem() {
    this.delete.emit(this.id);
  }

}

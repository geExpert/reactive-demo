export default class TodoItem implements TodoItemDocument {

  _id: TodoItemId;
  title: string;
  description: string;
  date: Date|string;
  status: string;
  __v: number;

  constructor(todoItemDocument: TodoItemDocument) {
    Object.assign(this as TodoItem, todoItemDocument);
    if (typeof todoItemDocument.date === 'string') {
      this.date = new Date(todoItemDocument.date);
    }
  }

  static createEmpty() {
    const todoItem = new TodoItem({} as TodoItemDocument);
    todoItem.title = "";
    todoItem.description = "";
    return todoItem;
  }

}

export const EMPTY_TODO_ITEM = TodoItem.createEmpty();

export type TodoItemId = string;

export interface TodoItemDocument {
  _id: TodoItemId;
  title: string;
  description: string;
  date: Date|string;
  status: string;
  __v: number;
}

export interface GenericResponse<T> {
  status: number;
  message: string;
  data : T
}

export interface TodoItemsResponse extends GenericResponse<{docs: TodoItemDocument[]}> {
}

export interface TodoItemResponse extends GenericResponse<TodoItemDocument>{
}
